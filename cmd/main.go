package main

import "ci-sample/pkg/sample"

func main() {
	router := sample.NewRouter()
	router.Logger.Fatal(router.Start(":80"))
}
